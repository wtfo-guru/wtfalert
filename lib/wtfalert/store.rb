# frozen_string_literal: true

require 'yaml'

#
# @module Wtfalert
#
module Wtfalert
  #
  # @class Store
  #
  class Store
    attr_reader :data_pathname

    def initialize(store)
      @stores = {}
      @data_pathname = nil
      @lockname = nil
      [store, ENV['HOME'], '/tmp'].each do |sd|
        spn = File.join(sd, 'alerts.yaml')
        @stores[spn] = check_store(sd, spn)
        if @stores[spn] == 'okay'
          @data_pathname = spn
          break
        end
      end
    end

    def save(data)
      raise 'Failed to find a valid data pathname!!!' if @data_pathname.nil?

      saved_mask = File.umask(0o0002)
      File.open(@data_pathname, 'w') do |file|
        file.write data.to_yaml
      end
    rescue StandardError => _e
      File.umask(saved_mask)
      raise
    end

    def load
      raise 'Failed to find a valid data pathname!!!' if @data_pathname.nil?

      dirty = false
      if File.exist?(@data_pathname)
        data, dirty = load_data(@data_pathname)
      else
        data = {}
        data[:created] = Time.now.to_s
        dirty = true
      end
      save(data) if dirty
      data
    end

    def lockname
      raise 'Failed to find a valid data pathname!!!' if @data_pathname.nil?

      return @lockname unless @lockname.nil?

      base = File.basename(@data_pathname)
      @lockname = File.join(File.dirname(@data_pathname), ".#{base}.lock")
    end

    private

    def load_data(fpn)
      dirty = false
      data = YAML.safe_load(File.read(fpn), [Symbol])
      if data.nil?
        data = {}
        data[:rescued] = Time.now.to_s
        dirty = true
      elsif data.key?('isondisk')
        data = convert(data)
        dirty = true
      end
      [data, dirty]
    end

    # rubocop:disable Metrics/AbcSize
    def convert(old)
      nh = {}
      nh[:created] = '¿Quién sabe?'
      nh[:converted] = Time.now.to_s
      old.each do |k, v|
        next if ['isondisk', 'puppet.run.failed'].include?(k) # orphaned alert
        next unless v.is_a?(Hash)
        next unless v.key?('count') && v.key?('throttled') && v.key?('last')

        nh[k] = {}
        nh[k][:count] = v['count']
        nh[k][:last] = v['last']
        nh[k][:throttled] = v['throttled']
      end
      nh
    end
    # rubocop:enable Metrics/AbcSize

    def check_store(parent, fpn)
      return 'Directory not found!' unless File.directory?(parent)

      if File.exist?(fpn)
        return 'File not readable!' unless File.readable?(fpn)
        return 'File not writable!' unless File.writable?(fpn)
      else
        return 'Directory not writable!' unless File.writable?(parent)
      end

      'okay'
    end
  end
end
