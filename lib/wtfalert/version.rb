# frozen_string_literal: true

#
# @module Wtfalert
#
module Wtfalert
  VERSION = '0.2.2'
end
